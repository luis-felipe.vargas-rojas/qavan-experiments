# QAVAN: <i>Query Answering Approach for Virtual Actionable Numerical Relationships</i>

## DBPedia & Eurostat experiment

This repository contains an executable jar of QAVAN and all the inputs to run the DBpedia & Eurostat experiment. It includes: data graph, shapes and ANRs, and SPARQL queries.

Execution example

```
java  -jar ../QAVAN-1.0-SNAPSHOT.jar --config "conf/conf_default_q1.json" --unzip --nestedANR --inferences

```