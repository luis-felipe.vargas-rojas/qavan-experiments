# Read in the file


with open(f'template.txt', 'r') as file :
  template = file.read() # to str

# Replace the target string
Q = ['1','2','3']
settings = ['default', 'nested', 'inference']


query_names = {"1":"q1_popDensity", "2":"q2_ratioMale", "3":"q3_rationFemale"}
   

for query_i in Q:

  query_name = query_names[query_i]


  for s in settings:

    instance = template
    instance = instance.replace('{QN}', query_name)
    instance = instance.replace('{Q}', "q"+query_i)
    instance = instance.replace('{M}', s)

    
    file_name = f'conf_{s}_q{query_i}.json'
    with open(file_name, 'wt') as fout :
        fout.write(instance)
  







