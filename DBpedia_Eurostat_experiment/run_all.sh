#! /usr/bin/env bash
# default

for filename in conf/conf_default*.json; do
     echo "$filename"
     java  -jar QAVAN-1.0-SNAPSHOT.jar --config "$filename"  --unzip
done

for filename in conf/conf_nested*.json; do
     echo "$filename"
     java  -jar QAVAN-1.0-SNAPSHOT.jar --config "$filename"  --unzip --nestedANR
done

for filename in conf/conf_inference*.json; do
     echo "$filename"
     java  -jar QAVAN-online-1.0-SNAPSHOT.jar --config "$filename"  --unzip --nestedANR --inferences
done


