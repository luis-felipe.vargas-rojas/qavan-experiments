# QAVAN: <i>Query Answering Approach for Virtual Actionable Numerical Relationships</i>

## Meteo-France experiment

This repository contains an executable jar of QAVAN and all the inputs to run the Meteo-France experiment. It includes: data graph, shapes and ANRs, and SPARQL queries.

Execution example

```
java  -jar ../QAVAN-1.0-SNAPSHOT.jar --config "conf/virtualN10R070Q01.json" 

```