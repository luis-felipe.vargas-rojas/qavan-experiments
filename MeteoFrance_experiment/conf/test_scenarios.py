# Read in the file

#method = "materialised"
method = "virtual"


with open(f'template_{method}.txt', 'r') as file :
  template = file.read() # to str

# Replace the target string
NStations = ['0','1','5','10','20','50']
NRules = ['5','10','20','30','40','50','60','70','80','90','100']
number_of_queries = 12

def get_query_type(i):
   if i in (0, 1, 2):
      return "ALL"
   elif i  in (3, 4, 5 ):
      return "ONE_DAY" 
   elif i  in (6, 7, 8 ):
      return "ONE_WEEK" 
   elif i  in (9, 10, 11 ):
      return "ONE_MONTH" 
   

for query_i in range(1, number_of_queries):

  query_type = get_query_type(query_i)

  query_name = "q"+ str(query_i)+"_"+query_type

  if(query_i<10):
            query_text = '0'+str(query_i)
  else: 
    query_text = str(query_i)

  for s in NStations:

      if(int(s)<10):
            s_text = '0'+s
      else: 
        s_text = s
      for r in NRules:
          instance = template
          instance = instance.replace('{NStations}', s)
          instance = instance.replace('{NRules}', r)
          instance = instance.replace('{QueryNumber}', str(query_i))
          instance = instance.replace('{QueryName}', query_name)

          if(int(r)<10):
            r = '00'+r
          else:
            if(int(r)!=100):
              r = '0'+r
          
          file_name = f'{method}N{s_text}R{r}Q{query_text}.json'
          with open(file_name, 'wt') as fout :
              fout.write(instance)
  







