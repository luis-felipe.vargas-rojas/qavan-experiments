#! /usr/bin/env bash
#SBATCH --output=/lustre/%u/logs/first-job-%j.out     # Output file
#SBATCH --error=/lustre/%u/logs/first-job-%j.err      # Error file

#SBATCH --mem=35000
#SBATCH --cpus-per-task=6


#SBATCH -A mistea    # GROUP
#SBATCH -p mistea     # PARTITION


if [  -z ${DATASET_ID+x} ] | [  -z ${SERVER+x} ] | [  -z ${MODEL+x} ]; 
then echo "Variables error  D = $DATASET_ID,  SERVER = $SERVER, MODEL = $MODEL"
exit  0;
else  echo "Running D = $DATASET_ID,  SERVER = $SERVER, MODEL = $MODEL"; fi

module load JDK/11.0.15

for filename in meteo_france/conf/$SERVER/$MODEL/virtualN$DATASET_ID*.json; do
     echo "$filename"
     java -Xms30g -Xmx30g -XX:+UseParallelGC -jar shacl-online-1.0-SNAPSHOT.jar --config "$filename" || true
done
